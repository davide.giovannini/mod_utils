//!
//! This crate contains some utils to simplify module declaration in rust code.
//!

#[macro_export]
/// Given a list of module names this macro expands to:
///
/// ```ignore
/// mod module1;
/// pub use self::module1::*;
///
/// ...
///
/// mod moduleN;
/// pub use self::moduleN::*;
/// ```
///
macro_rules! transparent_submodules {
    ($( $m:ident )*) => {

    $(
        mod $m;
        pub use self::$m::*;
    )*

    };
}
